﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace YC_Collect_Data
{
    public partial class SeleccionGrupos : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            int grupos = int.Parse(Session["grupos"].ToString());
            var list = (List<string>)Session["ar_ref"];
            string[] values = new string[list.Count];

            int i = 0;
            foreach (string referencia in list)
            {

                values[i] = referencia;
                var newPanel = new Panel();
                var newLabel = new Label();
                var newRadio = new RadioButtonList();

                for (int j=1; j <= grupos; j++)
                {
                    var newList = new ListItem();
                    newList.Value = "Grupo_" + j;
                    newRadio.Items.Add(newList);
                }

                newPanel.ID = "Panel_" + i;
                newLabel.ID = "Label_" + i;
                newRadio.ID = "RadioButtonList_" + i;
                newLabel.Text = " " + i.ToString() + ": " + referencia;

                newPanel.Controls.Add(new LiteralControl("<br />"));
                newPanel.Controls.Add(newLabel);
                newPanel.Controls.Add(newRadio);

                Pannel1.Controls.Add(newPanel);

                i++;
            }


        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Response.Redirect("CaptacionDatos.aspx");
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            SqlConnection conexion = new SqlConnection();
            conexion.ConnectionString = ConfigurationManager.ConnectionStrings[3].ConnectionString;


            for (int i = 0; i < int.Parse(Session["grupos"].ToString()); i++)
            {

                Panel panel = new Panel();
                try { panel = (Panel)form1.FindControl("Panel_" + i); }
                catch (Exception ex) { }
                Label label = new Label();
                try { label = (Label)panel.FindControl("Label_" + i); }
                catch (Exception ex) { }
                RadioButtonList radiobuttonlist = new RadioButtonList();
                try { radiobuttonlist = (RadioButtonList)panel.FindControl("RadioButtonList_" + i);} 
                catch (Exception ex) { }

                if (label.Text != null)
                {
                    try
                    {
                        conexion.Open();
                        Guid Id = new Guid();
                        string query = "INSERT INTO informes (id,nombre,tipo,grupo,posicion,fecha) VALUES(@param1,@param2,@param3,@param4,@param5,@param6)";
                        SqlCommand cmd = new SqlCommand(query, conexion);
                        cmd.Parameters.Add("@param1", SqlDbType.UniqueIdentifier).Value = Id;
                        cmd.Parameters.Add("@param2", SqlDbType.NVarChar, 255).Value = label.Text;
                        cmd.Parameters.Add("@param3", SqlDbType.Int, 50).Value = int.Parse(Session["RS"].ToString());
                        cmd.Parameters.Add("@param4", SqlDbType.NVarChar, 255).Value = radiobuttonlist.SelectedItem.Value.ToString();
                        cmd.Parameters.Add("@param5", SqlDbType.NVarChar, 255).Value = "delantero";
                        cmd.Parameters.Add("@param6", SqlDbType.DateTime).Value = DateTime.Parse(Session["fecha"].ToString());
                        cmd.ExecuteNonQuery();
                        conexion.Close();
                    }
                    catch (Exception ex)
                    {
                    }
                }

                Page.ClientScript.RegisterStartupScript(GetType(), "msgbox", "alert('Grupos guardados correctamente'); ", true);
              }

            }

            
        
    }
}