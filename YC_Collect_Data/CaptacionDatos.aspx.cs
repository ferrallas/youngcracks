﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace YC_Collect_Data
{
    public partial class Captacion_Datos : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            for (int i = 0; i < TotalNumberAdded; ++i)
            {
                AddControls(i + 1, 1);

            }

            Button1.Click += new EventHandler(Button1_Click);

        }

        void Button1_Click(object sender, EventArgs e)
        {

            TotalNumberAdded++;
            AddControls(TotalNumberAdded, 1);
        
        }

        private void AddControls(int controlNumber, int param)
        {
            var newPanel = new Panel();
            var newLabel = new Label();
            var newTextbox = new TextBox();
            var newCombo = new DropDownList();

            var newCombo2 = new DropDownList();
            newCombo2.Items.Insert(0, new ListItem("Benjamín", "1"));
            newCombo2.Items.Insert(0, new ListItem("Alevín", "2"));
            newCombo2.Items.Insert(0, new ListItem("Infantil", "3"));
            newCombo2.Items.Insert(0, new ListItem("Cadete", "4"));
            newCombo2.Items.Insert(0, new ListItem("Juvenil", "5"));
            newCombo2.ID = "Combo2_" + controlNumber;
            newCombo2.Width = 150;
            newCombo2.Height = 30;

            var newCombo3 = new DropDownList();
            newCombo3.Items.Insert(0, new ListItem("3", "3"));
            newCombo3.Items.Insert(0, new ListItem("2", "2"));
            newCombo3.Items.Insert(0, new ListItem("1", "1"));
            newCombo3.ID = "Combo3_" + controlNumber;
            newCombo3.Width = 150;
            newCombo3.Height = 30;

            if (Request.QueryString["RS"] == "1")
            {
                newCombo.Items.Insert(0, new ListItem("Portero", "1"));
            }

            if (Request.QueryString["RS"] == "2")
            {
                newCombo.Items.Insert(0, new ListItem("Defensa", "2"));
                newCombo.Items.Insert(0, new ListItem("Centro", "3"));
                newCombo.Items.Insert(0, new ListItem("Delantero", "4"));
            }

            newTextbox.ID = "TextBox_" + controlNumber;
            newTextbox.Width = 150;
            newTextbox.Height = 30;

            newCombo.ID = "Combo_" + controlNumber;
            newCombo.Width = 150;
            newCombo.Height = 30;

            newPanel.ID = "Panel_" + controlNumber;
            newLabel.Text = " " + controlNumber.ToString() + ": ";

            System.Web.UI.WebControls.ImageButton imagen = new System.Web.UI.WebControls.ImageButton();
            imagen.ID = "Img_" + controlNumber;
            imagen.Style["top"] = "1px";
            imagen.Width = 20;
            imagen.Height = 20;
            imagen.ImageUrl = "~/Images/delete.png";
            imagen.Click += new System.Web.UI.ImageClickEventHandler(Imagen_Click);

            newPanel.Controls.Add(new LiteralControl("<br />"));
            newPanel.Controls.Add(newLabel);
            newPanel.Controls.Add(newTextbox);
            newPanel.Controls.Add(newCombo);
            newPanel.Controls.Add(newCombo2);
            newPanel.Controls.Add(newCombo3);
            newPanel.Controls.Add(imagen);
            
            Pannel1.Controls.Add(newPanel);

        }



        void Imagen_Click(object sender, EventArgs e)
        {
            ImageButton img = (ImageButton)sender;
            string cod = img.UniqueID.ToString();
            cod = cod.Substring(4);
            Panel panel = (Panel)form1.FindControl("Panel_" + TotalNumberAdded);

            /* recoloca los valores, ids etc */

            for (int i = int.Parse(cod); i < TotalNumberAdded; i++)
            {
                TextBox tBox1 = (TextBox)form1.FindControl("TextBox_" + i);
                TextBox tBox2 = (TextBox)form1.FindControl("TextBox_" + (i + 1));
                tBox1.Text = tBox2.Text;

                DropDownList Combo1 = (DropDownList)form1.FindControl("Combo_" + i);
                DropDownList Combo2 = (DropDownList)form1.FindControl("Combo_" + (i + 1));
                Combo1.SelectedValue = Combo2.SelectedValue;

                Combo1 = (DropDownList)form1.FindControl("Combo2_" + i);
                Combo2 = (DropDownList)form1.FindControl("Combo2_" + (i + 1));
                Combo1.SelectedValue = Combo2.SelectedValue;

                Combo1 = (DropDownList)form1.FindControl("Combo3_" + i);
                Combo2 = (DropDownList)form1.FindControl("Combo3_" + (i + 1));
                Combo1.SelectedValue = Combo2.SelectedValue;
            }

            TotalNumberAdded = TotalNumberAdded - 1;
            panel.Controls.Clear();
        }


        protected int TotalNumberAdded
        {
            get { return (int)(ViewState["TotalNumberAdded"] ?? 0); }
            set { ViewState["TotalNumberAdded"] = value; }
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            List<string> ar_ref = new List<string>();
            for (int i = 1; i <= TotalNumberAdded; ++i)
            {
                TextBox tBox = (TextBox)form1.FindControl("TextBox_" + i);
                if (tBox.Text != "")
                {
                    ar_ref.Add(tBox.Text);
                }
            }

            Session["grupos"] = grupos.Text;
            Session["fecha"] = Datepic.SelectedDate;
            Session["ar_ref"] = ar_ref;

            if (Request.QueryString["RS"] == "1")
            {
                Session["RS"] = "1";
            }
            if (Request.QueryString["RS"] == "2")
            {
                Session["RS"] = "2";
            }

            if (Session["grupos"].ToString() != "" && Session["ar_ref"].ToString() != "")
            {
                Server.Transfer("SeleccionGrupos.aspx");
            }
            else 
            { 
            
            }


        }
    }
}