﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace YC_Collect_Data
{
    public partial class Grupop : System.Web.UI.Page
    {
        public object SavedChangesList { get; private set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            RadGrid1.DataSource = SqlDataSource1;


            SqlDataSource1.DeleteParameters.Add("@Id", ID.ToString());
            SqlDataSource1.DeleteCommand = "DELETE FROM [Informes] WHERE [ID] = @ID";

            SqlDataSource1.SelectParameters.Add("userId", ID.ToString());
            SqlDataSource1.SelectCommand = "SELECT [ID],[Nombre] FROM [Informes]";

            SqlDataSource1.InsertParameters.Add("userId", ID.ToString());
            SqlDataSource1.InsertCommand = "INSERT INTO [Orders] ([CustomerID], [EmployeeID], [OrderDate], [ShipName]) VALUES (@CustomerID, @EmployeeID, @OrderDate, @ShipName)";

            SqlDataSource1.UpdateParameters.Add("userId", ID.ToString());
            SqlDataSource1.UpdateCommand = "UPDATE [Informes] SET [ProductName] = @ProductName, [CategoryID] = @CategoryID, [UnitPrice] = @UnitPrice, [Discontinued] = @Discontinued, [QuantityPerUnit] = @QuantityPerUnit, [UnitsInStock] = @UnitsInStock WHERE [ProductID] = @ProductID";


            RadGrid2.DataSource = SqlDataSource1;
            RadGrid3.DataSource = SqlDataSource1;
            RadGrid4.DataSource = SqlDataSource1;
            RadGrid5.DataSource = SqlDataSource1;
            RadGrid6.DataSource = SqlDataSource1;
            RadGrid7.DataSource = SqlDataSource1;
            RadGrid8.DataSource = SqlDataSource1;
            RadGrid9.DataSource = SqlDataSource1;
        
        }

        protected void CheckBox2_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox chk = (CheckBox)sender;
            bool status = chk.Checked;
            //GridDataItem item = (GridDataItem)chk.NamingContainer;
            //string keyvalue = item.GetDataKeyValue("ProductName").ToString();
            //string connectionString = "";
            //if (status)
            //{

            //}
        }




    }
}