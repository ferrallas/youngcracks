﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="YC_Collect_Data._Default" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>


<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="jumbotron">
        <h1>Gestor 1.09</h1>
        <p class="lead"></p>
    </div>
    <input type="hidden" name="searchType" id="SearchType" />
    <div class="container-fluid">
    <div class="row">
            <h2>Youngcracks (C) <%: DateTime.Now.Year %> </h2>
            <div class="panel panel-default">
                <div class="panel-heading">
                Administrar Jugadores
            </div>
            <div class="panel-body">
               <div class="btn-group">
                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    Posici&oacuten <span class="caret"></span>
                </button>
                <ul class="dropdown-menu" role="menu">
                  <li><a href="CaptacionDatos.aspx?&RS=1">Porteros</a></li>
                  <li><a href="CaptacionDatos.aspx?&RS=2">Jugadores</a></li>
                </ul>
               </div>
              </div>
             </div>
            <br />
            <div class="panel panel-default">
            <div class="panel-heading">
            Insertar Fecha del Evento y Datos
            </div>
            <div class="panel-body">
               <input id="StartDate" class="datepicker" value="<%: DateTime.Today %>">
                <script type="text/javascript">
                    $('.datepicker').datepicker({
                        language: "es",
                        autoclose: true,
                        todayHighlight: true,
                        format : "dd/mm/yyyy"
                    });
                </script>
                <div class="btn-group">
                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                Posici&oacuten <span class="caret"></span>
                </button>
                <ul class="dropdown-menu" role="menu">
                  <li><a href="#" class="search-submit" data-search-type="1">Porteros</a></li>
                  <li><a href="#" class="search-submit" data-search-type="2">Jugadores</a></li>
                    <script type="text/javascript">
                        $('.search-submit').on('click', function (e) {
                            e.preventDefault();
                            var searchType = $(this).data('search-type');
                            $('#SearchType').val(searchType);
                            var grupos = "3";
                            if (searchType == 1)
                            {

                                //PageMethods.GetGrupos("1", $("#StartDate").val(), function (resultado) { grupos = resultado; }, function (resultado) { grupos = resultado; });
                               
                                $.ajax({
                                    type: "POST",
                                    async: false,
                                    url: "Default.aspx/GetGrupos",
                                    data: '{tipo: 1, StartDate: ' + $("#StartDate").val() + '}',
                                    contentType: "application/json; charset=utf-8",
                                    dataType: "json",
                                    success: function (result) {
                                        console.log("GetGrupos, ON: ");
                                        grupos = result.d;
                                    }
                                });
                                window.location = "Pruebas.aspx?&RS=1&Grupos=" + grupos + "&Fecha=" + $("#StartDate").val();
                            }
                            else
                            {
                                $.ajax({
                                    type: "POST",
                                    async: false,
                                    url: "Default.aspx/GetGrupos",
                                    data: '{tipo: 2, StartDate: ' + $("#StartDate").val() + '}',
                                    contentType: "application/json; charset=utf-8",
                                    dataType: "json",
                                    success: function (result) {
                                        console.log("GetGrupos, ON: ");
                                        grupos = result.d;
                                    }
                                });
                                window.location = "Pruebas.aspx?&RS=2&Grupos=" + grupos + "&Fecha=" + $("#StartDate").val()
                            }

                     
                        
                            $('#YourForm').submit();
                        });




             
                    </script>
                 </ul>
                </div>
             </div>
            </div>
          </div>
        </div>
 

</asp:Content>

