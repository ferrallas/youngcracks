﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Data;


namespace YC_Collect_Data
{
    public partial class Grupoj : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            RadGrid1.DataSource = SqlDataSource1;


            SqlDataSource1.DeleteParameters.Add("@Id", ID.ToString());
            SqlDataSource1.DeleteCommand = "DELETE FROM [Informes] WHERE [ID] = @ID";

            SqlDataSource1.SelectParameters.Add("userId", ID.ToString());
            SqlDataSource1.SelectCommand = "SELECT [ID],[Nombre] FROM [Informes]";

            SqlDataSource1.InsertParameters.Add("userId", ID.ToString());
            SqlDataSource1.InsertCommand = "INSERT INTO [Orders] ([CustomerID], [EmployeeID], [OrderDate], [ShipName]) VALUES (@CustomerID, @EmployeeID, @OrderDate, @ShipName)";

            SqlDataSource1.UpdateParameters.Add("userId", ID.ToString());
            SqlDataSource1.UpdateCommand = "UPDATE [Informes] SET [ProductName] = @ProductName, [CategoryID] = @CategoryID, [UnitPrice] = @UnitPrice, [Discontinued] = @Discontinued, [QuantityPerUnit] = @QuantityPerUnit, [UnitsInStock] = @UnitsInStock WHERE [ProductID] = @ProductID";


            RadGrid2.DataSource = SqlDataSource1;
            RadGrid3.DataSource = SqlDataSource1;
            RadGrid4.DataSource = SqlDataSource1;
            RadGrid5.DataSource = SqlDataSource1;
            RadGrid6.DataSource = SqlDataSource1;
            RadGrid7.DataSource = SqlDataSource1;
            RadGrid8.DataSource = SqlDataSource1;
            RadGrid9.DataSource = SqlDataSource1;
            RadGrid10.DataSource = SqlDataSource1;
            RadGrid11.DataSource = SqlDataSource1;
            RadGrid12.DataSource = SqlDataSource1;
        }

        protected void CheckBox2_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox chk = (CheckBox)sender;
            bool status = chk.Checked;
            //GridDataItem item = (GridDataItem)chk.NamingContainer;
            //string keyvalue = item.GetDataKeyValue("ProductName").ToString();
            //string connectionString = "";
            //if (status)
            //{

            //}
        }

        protected void RadGrid1_BatchEditCommand(object sender, Telerik.Web.UI.GridBatchEditingEventArgs e)
        {
            SavedChangesList.Visible = true;
        }

        protected void RadGrid1_ItemUpdated(object source, Telerik.Web.UI.GridUpdatedEventArgs e)
        {
            GridEditableItem item = (GridEditableItem)e.Item;
            String id = item.GetDataKeyValue("ProductID").ToString();
            if (e.Exception != null)
            {
                e.KeepInEditMode = true;
                e.ExceptionHandled = true;
                NotifyUser("Product with ID " + id + " cannot be updated. Reason: " + e.Exception.Message);
            }
            else
            {
                NotifyUser("Product with ID " + id + " is updated!");
            }
        }


        protected void RadGrid1_ItemDeleted(object source, GridDeletedEventArgs e)
        {
            GridDataItem dataItem = (GridDataItem)e.Item;
            String id = dataItem.GetDataKeyValue("ProductID").ToString();
            if (e.Exception != null)
            {
                e.ExceptionHandled = true;
                NotifyUser("Product with ID " + id + " cannot be deleted. Reason: " + e.Exception.Message);
            }
            else
            {
                NotifyUser("Product with ID " + id + " is deleted!");
            }

        }

        protected void RadGrid1_PreRender(object sender, EventArgs e)
        {
            RadNumericTextBox unitsNumericTextBox = (RadGrid1.MasterTableView.GetBatchColumnEditor("UnitsInStock") as GridNumericColumnEditor).NumericTextBox;
            unitsNumericTextBox.Width = Unit.Pixel(60);
        }

        private void NotifyUser(string message)
        {
            RadListBoxItem commandListItem = new RadListBoxItem();
            commandListItem.Text = message;
            SavedChangesList.Items.Add(commandListItem);
        }

       
    }
}