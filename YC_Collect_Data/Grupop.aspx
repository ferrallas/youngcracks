﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Grupop.aspx.cs" Inherits="YC_Collect_Data.Grupop" %>

<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>

<!DOCTYPE html>

<html lang="en">
<head>
<meta charset="UTF-8">
<title></title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<link href="css/scrolling-nav.css" rel="stylesheet">
<style type="text/css">
	.bs-example{
    	margin: 20px;
    }
    .RadGrid
    {
       border-radius: 10px;
       overflow: hidden;
    }
</style>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="The description of my page" />
</head>
<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">
    <form id="form1" runat="server">
        <asp:ScriptManager runat="server"></asp:ScriptManager>
<div class="bs-example">
    <nav id="myNavbar" class="navbar navbar-default" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="container">
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Seleccion de Pruebas (Portero)</a>
            </div>


            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li><a href="#">Grupo <%: + ' ' + Request.QueryString["GRN"] %> </a></li>
                </ul>
                <ul class="nav nav-tabs">
                    <li class="dropdown">
                        <a href="#" data-toggle="dropdown" class="dropdown-toggle">Prueba <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="#01" data-toggle="tab">20m lisos</a></li>
                            <li><a href="#02" data-toggle="tab">Salto vertical</a></li>
                            <li class="divider"></li>
                            <li><a href="#03" data-toggle="tab">Parada en el aire</a></li>
                            <li><a href="#04" data-toggle="tab">Parada disparo raso</a></li>
                            <li class="divider"></li>
                            <li><a href="#05" data-toggle="tab">Potencia saque pie</a></li>
                            <li><a href="#06" data-toggle="tab">Potencia saque mano</a></li>
                            <li><a href="#07" data-toggle="tab">Despeje puños</a></li>
                            <li><a href="#08" data-toggle="tab">Precisión saque porteria</a></li>
                            <li class="divider"></li>
                            <li><a href="#09" data-toggle="tab">Resumen</a></li>
                        </ul>
                    </li>
                </ul>
            </div><!-- /.navbar-collapse -->


             <div class="panel-body">
                    <div class="tab-content">
                        <div class="tab-pane fade in active" id="01">30m lisos
                             <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:YC_Collect_Data.Properties.Settings.Cadena2 %>"> </asp:SqlDataSource>
                            <telerik:RadGrid ID="RadGrid1" runat="server" RenderMode="Auto" AllowPaging="False" AutoGenerateColumns="False" ShowHeadersWhenNoRecords="true">  
                                <MasterTableView DataKeyNames="ID">
                                    <HeaderStyle Width="200px" />
                                    <Columns>
                                        <telerik:GridBoundColumn HeaderText="Nº" DataField="ID"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn HeaderText="Nombre" DataField="Name"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn HeaderText="Segs" DataField="Date"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn HeaderText="Decs" DataField="Date"></telerik:GridBoundColumn>
                                    </Columns>
                                    <CommandItemTemplate>
                                    </CommandItemTemplate>
                                </MasterTableView>
                                <ClientSettings AllowExpandCollapse="True">
                                    <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                                </ClientSettings>
                            </telerik:RadGrid>
                        </div>
                        <div class="tab-pane fade" id="02">Salto vertical
                            <telerik:RadGrid ID="RadGrid2" runat="server" RenderMode="Auto" AllowPaging="False" AutoGenerateColumns="False" ShowHeadersWhenNoRecords="true">  
                                <MasterTableView DataKeyNames="ID">
                                    <HeaderStyle Width="200px" />
                                    <Columns>
                                        <telerik:GridBoundColumn HeaderText="Nº" DataField="ID"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn HeaderText="Nombre" DataField="Name"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn HeaderText="Segs" DataField="Date"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn HeaderText="Decs" DataField="Date"></telerik:GridBoundColumn>
                                    </Columns>
                                    <CommandItemTemplate>
                                    </CommandItemTemplate>
                                </MasterTableView>
                                <ClientSettings AllowExpandCollapse="True">
                                    <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                                </ClientSettings>
                            </telerik:RadGrid>
                        </div>
                        <div class="tab-pane fade" id="03">Parada en el aire (lado derecho)
                           <%-- PASAR A DESLPEG>ABLE, LOS 3 PRIMEROS--%>
                           <telerik:RadGrid ID="RadGrid3" runat="server" RenderMode="Auto" AllowPaging="False" AutoGenerateColumns="False" ShowHeadersWhenNoRecords="true">  
                                <MasterTableView DataKeyNames="ID">
                                    <HeaderStyle Width="200px" />
                                    <Columns>
                                        <telerik:GridBoundColumn HeaderText="Nº" DataField="ID"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn HeaderText="Nombre" DataField="Name"></telerik:GridBoundColumn>
                                        <telerik:GridTemplateColumn HeaderText="1" HeaderStyle-Width="5%" UniqueName="1"> 
                                            <ItemTemplate>
                                           <telerik:RadButton ID="btnToggle1" runat="server" ToggleType="CustomToggle" ButtonType="StandardButton">
                                            <ToggleStates>
                                                <telerik:RadButtonToggleState Text="Falla" PrimaryIconCssClass="rbToggleCheckbox" />
                                                <telerik:RadButtonToggleState Text="Despeja" PrimaryIconCssClass="rbToggleCheckboxFilled" />
                                                <telerik:RadButtonToggleState Text="Empoma" PrimaryIconCssClass="rbToggleCheckboxChecked" CssClass="rbSkinnedButtonChecked" />
                                            </ToggleStates>
                                        </telerik:RadButton>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn> 
                                        <telerik:GridTemplateColumn HeaderText="2" HeaderStyle-Width="5%" UniqueName="2"> 
                                            <ItemTemplate>
                                           <telerik:RadButton ID="btnToggle2" runat="server" ToggleType="CustomToggle" ButtonType="StandardButton">
                                            <ToggleStates>
                                                <telerik:RadButtonToggleState Text="Falla" PrimaryIconCssClass="rbToggleCheckbox" />
                                                <telerik:RadButtonToggleState Text="Despeja" PrimaryIconCssClass="rbToggleCheckboxFilled" />
                                                <telerik:RadButtonToggleState Text="Empoma" PrimaryIconCssClass="rbToggleCheckboxChecked" CssClass="rbSkinnedButtonChecked" />
                                            </ToggleStates>
                                        </telerik:RadButton>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn> 
                                          <telerik:GridTemplateColumn HeaderText="3" HeaderStyle-Width="5%" UniqueName="3"> 
                                            <ItemTemplate>
                                           <telerik:RadButton ID="btnToggle3" runat="server" ToggleType="CustomToggle" ButtonType="StandardButton">
                                            <ToggleStates>
                                                <telerik:RadButtonToggleState Text="Falla" PrimaryIconCssClass="rbToggleCheckbox" />
                                                <telerik:RadButtonToggleState Text="Despeja" PrimaryIconCssClass="rbToggleCheckboxFilled" />
                                                <telerik:RadButtonToggleState Text="Empoma" PrimaryIconCssClass="rbToggleCheckboxChecked" CssClass="rbSkinnedButtonChecked" />
                                            </ToggleStates>
                                        </telerik:RadButton>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn> 
                                             <telerik:GridTemplateColumn HeaderText="4" HeaderStyle-Width="5%" UniqueName="4"> 
                                            <ItemTemplate>
                                           <telerik:RadButton ID="btnToggle4" runat="server" ToggleType="CustomToggle" ButtonType="StandardButton">
                                            <ToggleStates>
                                                <telerik:RadButtonToggleState Text="Falla" PrimaryIconCssClass="rbToggleCheckbox" />
                                                <telerik:RadButtonToggleState Text="Despeja" PrimaryIconCssClass="rbToggleCheckboxFilled" />
                                                <telerik:RadButtonToggleState Text="Empoma" PrimaryIconCssClass="rbToggleCheckboxChecked" CssClass="rbSkinnedButtonChecked" />
                                            </ToggleStates>
                                        </telerik:RadButton>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn> 
                                         <telerik:GridTemplateColumn HeaderText="5" HeaderStyle-Width="5%" UniqueName="5"> 
                                            <ItemTemplate>
                                           <telerik:RadButton ID="btnToggle5" runat="server" ToggleType="CustomToggle" ButtonType="StandardButton">
                                            <ToggleStates>
                                                <telerik:RadButtonToggleState Text="Falla" PrimaryIconCssClass="rbToggleCheckbox" />
                                                <telerik:RadButtonToggleState Text="Despeja" PrimaryIconCssClass="rbToggleCheckboxFilled" />
                                                <telerik:RadButtonToggleState Text="Empoma" PrimaryIconCssClass="rbToggleCheckboxChecked" CssClass="rbSkinnedButtonChecked" />
                                            </ToggleStates>
                                        </telerik:RadButton>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn> 
                                         <telerik:GridTemplateColumn HeaderText="6" HeaderStyle-Width="5%" UniqueName="6"> 
                                            <ItemTemplate>
                                           <telerik:RadButton ID="btnToggle6" runat="server" ToggleType="CustomToggle" ButtonType="StandardButton">
                                            <ToggleStates>
                                                <telerik:RadButtonToggleState Text="Falla" PrimaryIconCssClass="rbToggleCheckbox" />
                                                <telerik:RadButtonToggleState Text="Despeja" PrimaryIconCssClass="rbToggleCheckboxFilled" />
                                                <telerik:RadButtonToggleState Text="Empoma" PrimaryIconCssClass="rbToggleCheckboxChecked" CssClass="rbSkinnedButtonChecked" />
                                            </ToggleStates>
                                        </telerik:RadButton>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn> 
                                        <telerik:GridBoundColumn HeaderText="Observaciones" UniqueName="Observaciones"></telerik:GridBoundColumn>
                                    </Columns>
                                    <CommandItemTemplate>
                                    </CommandItemTemplate>
                                </MasterTableView>
                                <ClientSettings AllowExpandCollapse="True">
                                    <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                                </ClientSettings>
                            </telerik:RadGrid>
                        </div>
                <%--        Parada en el aire (lado izquierdo)--%>
                        <div class="tab-pane fade" id="04">Parada disparo raso (lado derecho)
                           <telerik:RadGrid ID="RadGrid4" runat="server" RenderMode="Auto" AllowPaging="False" AutoGenerateColumns="False" ShowHeadersWhenNoRecords="true">  
                                <MasterTableView DataKeyNames="ID">
                                    <HeaderStyle Width="200px" />
                                    <Columns>
                                        <telerik:GridBoundColumn HeaderText="Nº" DataField="ID"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn HeaderText="Nombre" DataField="Name"></telerik:GridBoundColumn>
                                        <telerik:GridTemplateColumn HeaderText="1" HeaderStyle-Width="5%" UniqueName="1"> 
                                            <ItemTemplate>
                                           <telerik:RadButton ID="btnToggle8" runat="server" ToggleType="CustomToggle" ButtonType="StandardButton">
                                            <ToggleStates>
                                                <telerik:RadButtonToggleState Text="Falla" PrimaryIconCssClass="rbToggleCheckbox" />
                                                <telerik:RadButtonToggleState Text="Despeja" PrimaryIconCssClass="rbToggleCheckboxFilled" />
                                                <telerik:RadButtonToggleState Text="Empoma" PrimaryIconCssClass="rbToggleCheckboxChecked" CssClass="rbSkinnedButtonChecked" />
                                            </ToggleStates>
                                        </telerik:RadButton>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn> 
                                        <telerik:GridTemplateColumn HeaderText="2" HeaderStyle-Width="5%" UniqueName="2"> 
                                            <ItemTemplate>
                                           <telerik:RadButton ID="btnToggle9" runat="server" ToggleType="CustomToggle" ButtonType="StandardButton">
                                            <ToggleStates>
                                                <telerik:RadButtonToggleState Text="Falla" PrimaryIconCssClass="rbToggleCheckbox" />
                                                <telerik:RadButtonToggleState Text="Despeja" PrimaryIconCssClass="rbToggleCheckboxFilled" />
                                                <telerik:RadButtonToggleState Text="Empoma" PrimaryIconCssClass="rbToggleCheckboxChecked" CssClass="rbSkinnedButtonChecked" />
                                            </ToggleStates>
                                        </telerik:RadButton>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn> 
                                          <telerik:GridTemplateColumn HeaderText="3" HeaderStyle-Width="5%" UniqueName="3"> 
                                            <ItemTemplate>
                                           <telerik:RadButton ID="btnToggle10" runat="server" ToggleType="CustomToggle" ButtonType="StandardButton">
                                            <ToggleStates>
                                                <telerik:RadButtonToggleState Text="Falla" PrimaryIconCssClass="rbToggleCheckbox" />
                                                <telerik:RadButtonToggleState Text="Despeja" PrimaryIconCssClass="rbToggleCheckboxFilled" />
                                                <telerik:RadButtonToggleState Text="Empoma" PrimaryIconCssClass="rbToggleCheckboxChecked" CssClass="rbSkinnedButtonChecked" />
                                            </ToggleStates>
                                        </telerik:RadButton>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn> 
                                             <telerik:GridTemplateColumn HeaderText="4" HeaderStyle-Width="5%" UniqueName="4"> 
                                            <ItemTemplate>
                                           <telerik:RadButton ID="btnToggle11" runat="server" ToggleType="CustomToggle" ButtonType="StandardButton">
                                            <ToggleStates>
                                                <telerik:RadButtonToggleState Text="Falla" PrimaryIconCssClass="rbToggleCheckbox" />
                                                <telerik:RadButtonToggleState Text="Despeja" PrimaryIconCssClass="rbToggleCheckboxFilled" />
                                                <telerik:RadButtonToggleState Text="Empoma" PrimaryIconCssClass="rbToggleCheckboxChecked" CssClass="rbSkinnedButtonChecked" />
                                            </ToggleStates>
                                        </telerik:RadButton>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn> 
                                         <telerik:GridTemplateColumn HeaderText="5" HeaderStyle-Width="5%" UniqueName="5"> 
                                            <ItemTemplate>
                                           <telerik:RadButton ID="btnToggle12" runat="server" ToggleType="CustomToggle" ButtonType="StandardButton">
                                            <ToggleStates>
                                                <telerik:RadButtonToggleState Text="Falla" PrimaryIconCssClass="rbToggleCheckbox" />
                                                <telerik:RadButtonToggleState Text="Despeja" PrimaryIconCssClass="rbToggleCheckboxFilled" />
                                                <telerik:RadButtonToggleState Text="Empoma" PrimaryIconCssClass="rbToggleCheckboxChecked" CssClass="rbSkinnedButtonChecked" />
                                            </ToggleStates>
                                        </telerik:RadButton>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn> 
                                         <telerik:GridTemplateColumn HeaderText="6" HeaderStyle-Width="5%" UniqueName="6"> 
                                            <ItemTemplate>
                                           <telerik:RadButton ID="btnToggle13" runat="server" ToggleType="CustomToggle" ButtonType="StandardButton">
                                            <ToggleStates>
                                                <telerik:RadButtonToggleState Text="Falla" PrimaryIconCssClass="rbToggleCheckbox" />
                                                <telerik:RadButtonToggleState Text="Despeja" PrimaryIconCssClass="rbToggleCheckboxFilled" />
                                                <telerik:RadButtonToggleState Text="Empoma" PrimaryIconCssClass="rbToggleCheckboxChecked" CssClass="rbSkinnedButtonChecked" />
                                            </ToggleStates>
                                        </telerik:RadButton>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn> 
                                        <telerik:GridBoundColumn HeaderText="Observaciones" UniqueName="Observaciones"></telerik:GridBoundColumn>
                                    </Columns>
                                    <CommandItemTemplate>
                                    </CommandItemTemplate>
                                </MasterTableView>
                                <ClientSettings AllowExpandCollapse="True">
                                    <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                                </ClientSettings>
                            </telerik:RadGrid>
                        </div>
                        <div class="tab-pane fade" id="05">Potencia saque pie
                           <telerik:RadGrid ID="RadGrid5" runat="server" RenderMode="Auto" AllowPaging="False" AutoGenerateColumns="False" ShowHeadersWhenNoRecords="true">  
                                <MasterTableView DataKeyNames="ID">
                                    <HeaderStyle Width="200px" />
                                    <Columns>
                                        <telerik:GridBoundColumn HeaderText="Nº" DataField="ID"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn HeaderText="Nombre" DataField="Name"></telerik:GridBoundColumn>
                                         <telerik:GridTemplateColumn HeaderText="1" HeaderStyle-Width="5%" UniqueName="1"> 
                                            <ItemTemplate>
                                                 <telerik:RadComboBox  id="RadComboBox112" 
                                                                        runat="server"> 
                                                    <Items>   
                                                        <telerik:RadComboBoxItem runat="server" Text="0" />   
                                                        <telerik:RadComboBoxItem runat="server" Text="A" />   
                                                        <telerik:RadComboBoxItem runat="server" Text="B" /> 
                                                         <telerik:RadComboBoxItem runat="server" Text="C" /> 
                                                         <telerik:RadComboBoxItem runat="server" Text="D" /> 
                                                    </Items>
                                                </telerik:RadComboBox>
                                                </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                         <telerik:GridTemplateColumn HeaderText="2" HeaderStyle-Width="5%" UniqueName="2"> 
                                            <ItemTemplate>
                                                 <telerik:RadComboBox  id="RadComboBox322" 
                                                                        runat="server"> 
                                                    <Items>   
                                                        <telerik:RadComboBoxItem runat="server" Text="0" />   
                                                        <telerik:RadComboBoxItem runat="server" Text="A" />   
                                                        <telerik:RadComboBoxItem runat="server" Text="B" /> 
                                                         <telerik:RadComboBoxItem runat="server" Text="C" /> 
                                                    <telerik:RadComboBoxItem runat="server" Text="D" /> 
                                                           </Items>
                                                </telerik:RadComboBox>
                                                </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                    </Columns>
                                    <CommandItemTemplate>
                                    </CommandItemTemplate>
                                </MasterTableView>
                                <ClientSettings AllowExpandCollapse="True">
                                    <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                                </ClientSettings>
                            </telerik:RadGrid>
                        </div>
                        <div class="tab-pane fade" id="06">Potencia saque mano
                           <telerik:RadGrid ID="RadGrid6" runat="server" RenderMode="Auto" AllowPaging="False" AutoGenerateColumns="False" ShowHeadersWhenNoRecords="true">  
                                <MasterTableView DataKeyNames="ID">
                                    <HeaderStyle Width="200px" />
                                    <Columns>
                                        <telerik:GridBoundColumn HeaderText="Nº" DataField="ID"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn HeaderText="Nombre" DataField="Name"></telerik:GridBoundColumn>
                                         <telerik:GridTemplateColumn HeaderText="1" HeaderStyle-Width="5%" UniqueName="1"> 
                                            <ItemTemplate>
                                                 <telerik:RadComboBox  id="RadComboBox112" 
                                                                        runat="server"> 
                                                    <Items>   
                                                        <telerik:RadComboBoxItem runat="server" Text="0" />   
                                                        <telerik:RadComboBoxItem runat="server" Text="A" />   
                                                        <telerik:RadComboBoxItem runat="server" Text="B" /> 
                                                         <telerik:RadComboBoxItem runat="server" Text="C" /> 
                                                         <telerik:RadComboBoxItem runat="server" Text="D" /> 
                                                    </Items>
                                                </telerik:RadComboBox>
                                                </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                         <telerik:GridTemplateColumn HeaderText="2" HeaderStyle-Width="5%" UniqueName="2"> 
                                            <ItemTemplate>
                                                 <telerik:RadComboBox  id="RadComboBox322" 
                                                                        runat="server"> 
                                                    <Items>   
                                                        <telerik:RadComboBoxItem runat="server" Text="0" />   
                                                        <telerik:RadComboBoxItem runat="server" Text="A" />   
                                                        <telerik:RadComboBoxItem runat="server" Text="B" /> 
                                                         <telerik:RadComboBoxItem runat="server" Text="C" /> 
                                                           <telerik:RadComboBoxItem runat="server" Text="D" /> 
                                                    </Items>
                                                </telerik:RadComboBox>
                                                </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                          <telerik:GridTemplateColumn HeaderText="BLOCAJE" HeaderStyle-Width="5%" UniqueName="BLOCAJE"> 
                                              <ItemTemplate>
                                            <telerik:RadButton ID="RadButton2" runat="server" ToggleType="CheckBox" ButtonType="LinkButton">
                                            <ToggleStates>
                                                <telerik:RadButtonToggleState Text="SI" PrimaryIconCssClass="rbToggleCheckboxChecked" />
                                                <telerik:RadButtonToggleState Text="NO" PrimaryIconCssClass="rbToggleCheckbox" />
                                            </ToggleStates>
                                        </telerik:RadButton>
                                       </ItemTemplate>
                                        </telerik:GridTemplateColumn> 
                                    </Columns>
                                    <CommandItemTemplate>
                                    </CommandItemTemplate>
                                </MasterTableView>
                                <ClientSettings AllowExpandCollapse="True">
                                    <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                                </ClientSettings>
                            </telerik:RadGrid>
                        </div>
                        <div class="tab-pane fade" id="07">Despeje puños
                            <telerik:RadGrid ID="RadGrid7" runat="server" RenderMode="Auto" AllowPaging="False" AutoGenerateColumns="False" ShowHeadersWhenNoRecords="true">  
                                <MasterTableView DataKeyNames="ID">
                                    <HeaderStyle Width="200px" />
                                    <Columns>
                            <%--            PASAR A COMBO--%>
                                        <telerik:GridBoundColumn HeaderText="Nº" DataField="ID"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn HeaderText="Nombre" DataField="Name"></telerik:GridBoundColumn>
                                         <telerik:GridTemplateColumn HeaderText="1" HeaderStyle-Width="5%" UniqueName="1"> 
                                            <ItemTemplate>
                                           <telerik:RadButton ID="btnToggle14" runat="server" ToggleType="CustomToggle" ButtonType="StandardButton">
                                            <ToggleStates>
                                                <telerik:RadButtonToggleState Text="0" PrimaryIconCssClass="rbToggleCheckbox" />
                                                <telerik:RadButtonToggleState Text="A" PrimaryIconCssClass="rbToggleCheckboxFilled" />
                                                <telerik:RadButtonToggleState Text="B" PrimaryIconCssClass="rbToggleCheckboxChecked" CssClass="rbSkinnedButtonChecked" />
                                            </ToggleStates>
                                        </telerik:RadButton>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn> 
                                         <telerik:GridTemplateColumn HeaderText="2" HeaderStyle-Width="5%" UniqueName="2"> 
                                            <ItemTemplate>
                                           <telerik:RadButton ID="btnToggle16" runat="server" ToggleType="CustomToggle" ButtonType="StandardButton">
                                            <ToggleStates>
                                                <telerik:RadButtonToggleState Text="0" PrimaryIconCssClass="rbToggleCheckbox" />
                                                <telerik:RadButtonToggleState Text="A" PrimaryIconCssClass="rbToggleCheckboxFilled" />
                                                <telerik:RadButtonToggleState Text="B" PrimaryIconCssClass="rbToggleCheckboxChecked" CssClass="rbSkinnedButtonChecked" />
                                            </ToggleStates>
                                        </telerik:RadButton>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn> 
                                         <telerik:GridTemplateColumn HeaderText="3" HeaderStyle-Width="5%" UniqueName="3"> 
                                            <ItemTemplate>
                                           <telerik:RadButton ID="btnToggle17" runat="server" ToggleType="CustomToggle" ButtonType="StandardButton">
                                            <ToggleStates>
                                                <telerik:RadButtonToggleState Text="0" PrimaryIconCssClass="rbToggleCheckbox" />
                                                <telerik:RadButtonToggleState Text="A" PrimaryIconCssClass="rbToggleCheckboxFilled" />
                                                <telerik:RadButtonToggleState Text="B" PrimaryIconCssClass="rbToggleCheckboxChecked" CssClass="rbSkinnedButtonChecked" />
                                            </ToggleStates>
                                        </telerik:RadButton>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn> 
                                       
                                        <telerik:GridBoundColumn HeaderText="Observaciones" DataField="Observaciones"></telerik:GridBoundColumn>
                                    </Columns>
                                    <CommandItemTemplate>
                                    </CommandItemTemplate>
                                </MasterTableView>
                                <ClientSettings AllowExpandCollapse="True">
                                    <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                                </ClientSettings>
                            </telerik:RadGrid>
                        </div>
                        <div class="tab-pane fade" id="08">Precisión saque porteria
         <%--                   PASAR A COMBO
                            DIRECTO, BOTE, FALLO--%>
                           <telerik:RadGrid ID="RadGrid8" runat="server" RenderMode="Auto" AllowPaging="False" AutoGenerateColumns="False" ShowHeadersWhenNoRecords="true">  
                                <MasterTableView DataKeyNames="ID">
                                    <HeaderStyle Width="200px" />
                                    <Columns>
                                        <telerik:GridBoundColumn HeaderText="Nº" DataField="ID"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn HeaderText="Nombre" DataField="Name"></telerik:GridBoundColumn>
                                         <telerik:GridTemplateColumn HeaderText="1" HeaderStyle-Width="5%" UniqueName="1"> 
                                                   <ItemTemplate>
                                                                         
                                            <telerik:RadButton ID="RadButton3" runat="server" ToggleType="CheckBox" ButtonType="LinkButton">
                                            <ToggleStates>
                                                <telerik:RadButtonToggleState Text="1" PrimaryIconCssClass="rbToggleCheckboxChecked" />
                                                <telerik:RadButtonToggleState Text="1" PrimaryIconCssClass="rbToggleCheckbox" />
                                            </ToggleStates>
                                        </telerik:RadButton>
                                                                                    </ItemTemplate>
                                        </telerik:GridTemplateColumn> 
                                         <telerik:GridTemplateColumn HeaderText="2" HeaderStyle-Width="5%" UniqueName="2"> 
                                                   <ItemTemplate>
                                                                         
                                            <telerik:RadButton ID="RadButton3" runat="server" ToggleType="CheckBox" ButtonType="LinkButton">
                                            <ToggleStates>
                                                <telerik:RadButtonToggleState Text="2" PrimaryIconCssClass="rbToggleCheckboxChecked" />
                                                <telerik:RadButtonToggleState Text="2" PrimaryIconCssClass="rbToggleCheckbox" />
                                            </ToggleStates>
                                        </telerik:RadButton>
                                                                                    </ItemTemplate>
                                        </telerik:GridTemplateColumn> 
                                         <telerik:GridTemplateColumn HeaderText="3" HeaderStyle-Width="5%" UniqueName="3"> 
                                                   <ItemTemplate>
                                                                         
                                            <telerik:RadButton ID="RadButton3" runat="server" ToggleType="CheckBox" ButtonType="LinkButton">
                                            <ToggleStates>
                                                <telerik:RadButtonToggleState Text="3" PrimaryIconCssClass="rbToggleCheckboxChecked" />
                                                <telerik:RadButtonToggleState Text="3" PrimaryIconCssClass="rbToggleCheckbox" />
                                            </ToggleStates>
                                        </telerik:RadButton>
                                                                                    </ItemTemplate>
                                        </telerik:GridTemplateColumn> 
                                        <telerik:GridBoundColumn HeaderText="Observaciones" DataField="Observaciones"></telerik:GridBoundColumn>
                                    </Columns>
                                    <CommandItemTemplate>
                                    </CommandItemTemplate>
                                </MasterTableView>
                                <ClientSettings AllowExpandCollapse="True">
                                    <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                                </ClientSettings>
                            </telerik:RadGrid>
                        </div>
                        <div class="tab-pane fade" id="09">Resumen
                           <telerik:RadGrid ID="RadGrid9" runat="server" RenderMode="Auto" AllowPaging="False" AutoGenerateColumns="False" ShowHeadersWhenNoRecords="true">  
                                <MasterTableView DataKeyNames="ID">
                                    <HeaderStyle Width="200px" />
                                    <Columns>
                                        <telerik:GridBoundColumn HeaderText="Nº" DataField="ID"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn HeaderText="Nombre" DataField="Name"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn HeaderText="Segs" DataField="Date"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn HeaderText="Decs" DataField="Date"></telerik:GridBoundColumn>
                                    </Columns>
                                    <CommandItemTemplate>
                                    </CommandItemTemplate>
                                </MasterTableView>
                                <ClientSettings AllowExpandCollapse="True">
                                    <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                                </ClientSettings>
                            </telerik:RadGrid>
                        </div>
                    </div>
             </div>

        </div>
    </nav>
</div>
   
    </form>
</body>
</html>                                		