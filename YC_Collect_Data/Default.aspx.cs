﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Globalization;

namespace YC_Collect_Data
{
    public partial class _Default : Page
    {
       
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [System.Web.Services.WebMethod]
        public static string GetGrupos(int tipo, string StartDate)
        {
            string grupos = "0";

            string conexion = ConfigurationManager.ConnectionStrings[3].ConnectionString;
            using (SqlConnection conn = new SqlConnection(conexion))
            {
                try
                {
                    conn.Open();
                    string query = "SELECT DISTINCT COUNT (*) FROM informes WHERE fecha = @date AND tipo = @tipo";

                    SqlCommand cmd = new SqlCommand(query, conn);
                    cmd.Parameters.AddWithValue("@date", DateTime.Parse(StartDate.ToString()));
                    cmd.Parameters.AddWithValue("@tipo", tipo);

                    grupos = cmd.ExecuteScalar().ToString();
                    conn.Close();
                }
                catch (Exception ex)
                {
                   
                }
            }
            return grupos;
        }



       
    }
}