﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(YC_Collect_Data.Startup))]
namespace YC_Collect_Data
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
