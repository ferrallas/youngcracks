﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CaptacionDatos.aspx.cs" Inherits="YC_Collect_Data.Captacion_Datos" %>

<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <meta charset="utf-8">
    <title></title>
        <link href="Css/jquery-ui.css" rel="stylesheet">
        <script src="Js/jquery-1.11.1.min.js"></script>
</head>
<body>
<form id="form1" runat="server">
    <asp:ScriptManager runat="server" EnablePageMethods="True">
    </asp:ScriptManager>
    <asp:Panel ID="PanelControles" runat="server">

          Nº de Grupos
          <asp:TextBox ID="grupos" runat="server" Width="52px"></asp:TextBox>
          <br />
          <br />
          Fecha del Evento
          <telerik:RadDatePicker ID="Datepic" Runat="server" Width="165px" WrapperTableCaption="">
          </telerik:RadDatePicker>
          <br />
          <br />
          <asp:Button ID="Button1" runat="server" Text="Añadir Jugador" />
   </asp:Panel>
<fieldset>
<legend id="Ref1">Introduzca los datos</legend>
    <asp:Panel ID="Pannel1" runat="server">
        <asp:TextBox ID="referencias" runat="server" Width="150px" Height="30px" Visible="False"></asp:TextBox>
    </asp:Panel>
</fieldset>
<div style="padding-top: 10px;">
 <span style="margin-left: 10px"> <asp:Button ID="Button2" CssClass="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only ui-state-hover" runat="server" Text="Ok" onclick="Button2_Click" /> </div>
<div style="padding: 5px; height: 68px;">
</div>

</form>
</body>
</html>
