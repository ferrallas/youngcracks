﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Grupoj.aspx.cs" Inherits="YC_Collect_Data.Grupoj" %>

<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagprefix="telerik" %>

<!DOCTYPE html>

<html lang="en">
<head>
<meta charset="UTF-8">
<title></title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<link href="css/scrolling-nav.css" rel="stylesheet">
<style type="text/css">
	.bs-example{
    	margin: 20px;
    }
    .RadGrid
    {
       border-radius: 10px;
       overflow: hidden;
    }
</style>
</head>
<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">
    <form id="form1" runat="server">
        <asp:ScriptManager runat="server"></asp:ScriptManager>
<div class="bs-example">
    <nav id="myNavbar" class="navbar navbar-default" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="container">
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Seleccion de Pruebas (Jugador)</a>
            </div>


            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li>
                        <asp:PlaceHolder runat="server"><a href="#">Grupo <%: + ' ' + Request.QueryString["GRN"] %> </a></asp:PlaceHolder></li>
                </ul>
                <ul class="nav nav-tabs">
                    <li class="dropdown">
                        <a href="#" data-toggle="dropdown" class="dropdown-toggle">Prueba <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="#01" data-toggle="tab">Course Navette</a></li>
                            <li><a href="#02" data-toggle="tab">20m lisos</a></li>
                            <li><a href="#03" data-toggle="tab">Salto vertical</a></li>
                            <li class="divider"></li>
                            <li><a href="#04" data-toggle="tab">Disparo P.Dominante</a></li>
                            <li><a href="#05" data-toggle="tab">Disparo P. NO Dominante</a></li>
                            <li><a href="#06" data-toggle="tab">Pase en carrera</a></li>
                            <li><a href="#07" data-toggle="tab">Toques balón</a></li>
                            <li><a href="#08" data-toggle="tab">Circuito</a></li>
                            <li class="divider"></li>
                            <li><a href="#09" data-toggle="tab">Remate de cabeza</a></li>
                            <li><a href="#10" data-toggle="tab">Remate con el pie</a></li>
                            <li><a href="#11" data-toggle="tab">Potencia de disparo</a></li>
                            <li class="divider"></li>
                            <li><a href="#12" data-toggle="tab">Resumen</a></li>
                        </ul>
                    </li>
                </ul>
            </div><!-- /.navbar-collapse -->


             <div class="panel-body">
                    <div class="tab-content">
                        <div class="tab-pane fade in active" id="01">Course Navette
                                <telerik:RadAjaxManager runat="server">
                                <AjaxSettings>
                                    <telerik:AjaxSetting AjaxControlID="RadGrid1">
                                        <UpdatedControls>
                                            <telerik:AjaxUpdatedControl ControlID="RadGrid1" LoadingPanelID="RadAjaxLoadingPanel1" />
                                            <telerik:AjaxUpdatedControl ControlID="SavedChangesList" />
                                        </UpdatedControls>
                                    </telerik:AjaxSetting>
                                    <telerik:AjaxSetting AjaxControlID="ConfigurationPanel1">
                                        <UpdatedControls>
                                            <telerik:AjaxUpdatedControl ControlID="RadGrid1" LoadingPanelID="RadAjaxLoadingPanel1" />
                                        </UpdatedControls>
                                    </telerik:AjaxSetting>
                                </AjaxSettings>
                            </telerik:RadAjaxManager>
                            <telerik:RadAjaxLoadingPanel runat="server" ID="RadAjaxLoadingPanel1"></telerik:RadAjaxLoadingPanel>
                            <telerik:RadListBox runat="server" ID="SavedChangesList" Width="600px" Height="200px" Visible="false"></telerik:RadListBox>
                            <telerik:RadGrid ID="RadGrid1" runat="server" RenderMode="Auto" AllowPaging="False" AutoGenerateColumns="False" ShowHeadersWhenNoRecords="true"
                                AllowAutomaticInserts="True" AllowAutomaticUpdates="True" AllowAutomaticDeletes="True">
                                <MasterTableView DataKeyNames="ID" CommandItemDisplay="Top" EditMode="Batch" BatchEditingSettings-OpenEditingEvent="Click" 
                                    BatchEditingSettings-EditType="Row">
                                    <HeaderStyle Width="200px" />
                                    <Columns>
                                        <telerik:GridBoundColumn HeaderText="Nº" DataField="ID" UniqueName="ID" HeaderStyle-Width="5%"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn HeaderText="Nombre" UniqueName="Nombre" DataField="Nombre" HeaderStyle-Width="40%"></telerik:GridBoundColumn>
                                        <telerik:GridNumericColumn DataField="Periodo" HeaderStyle-Width="50%" HeaderText="Periodo"
                                            SortExpression="Periodo" UniqueName="Periodo">
                                        </telerik:GridNumericColumn>
                                        <telerik:GridTemplateColumn HeaderText="Ok" HeaderStyle-Width="5%" UniqueName="Ok"> 
                                            <ItemTemplate> 
                                                <asp:CheckBox ID="CheckBox2" runat="server" AutoPostBack="true" OnCheckedChanged="CheckBox2_CheckedChanged" />
                                            </ItemTemplate> 
                                        </telerik:GridTemplateColumn> 
                                        <telerik:GridBoundColumn HeaderText="Observaciones" UniqueName="Observaciones"></telerik:GridBoundColumn>
                                    </Columns>
                                </MasterTableView>
                                <ClientSettings AllowExpandCollapse="True" AllowKeyboardNavigation="true">
                                    <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                                </ClientSettings>
                            </telerik:RadGrid>
                            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:YC_Collect_Data.Properties.Settings.Cadena2 %>"> </asp:SqlDataSource>
                        </div>
                        <div class="tab-pane fade" id="02">30m lisos
                            <telerik:RadGrid ID="RadGrid2" runat="server" RenderMode="Auto" AllowPaging="False" AutoGenerateColumns="False" ShowHeadersWhenNoRecords="true">  
                                <MasterTableView DataKeyNames="ID">
                                    <HeaderStyle Width="200px" />
                                    <Columns>
                                        <telerik:GridBoundColumn HeaderText="Nº" DataField="ID"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn HeaderText="Nombre" Datafield="Nombre"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn HeaderText="Segs" DataField="Segs"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn HeaderText="Decs" DataField="Decs"></telerik:GridBoundColumn>
                                         <telerik:GridBoundColumn HeaderText="Observaciones" UniqueName="Observaciones"></telerik:GridBoundColumn>
                                    </Columns>
                                    <CommandItemTemplate>
                                    </CommandItemTemplate>
                                </MasterTableView>
                                <ClientSettings AllowExpandCollapse="True">
                                    <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                                </ClientSettings>
                            </telerik:RadGrid>
                        </div>
                        <div class="tab-pane fade" id="03">Salto vertical
                            <telerik:RadGrid ID="RadGrid3" runat="server" RenderMode="Auto" AllowPaging="False" AutoGenerateColumns="False" ShowHeadersWhenNoRecords="true">  
                                <MasterTableView DataKeyNames="ID">
                                    <HeaderStyle Width="200px" />
                                    <Columns>
                                        <telerik:GridBoundColumn HeaderText="Nº" DataField="ID"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn HeaderText="Nombre" Datafield="Nombre"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn HeaderText="De Pie (cm)" DataField="DePie"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn HeaderText="Salto (cm)" DataField="Salto"></telerik:GridBoundColumn>
                                         <telerik:GridBoundColumn HeaderText="Observaciones" UniqueName="Observaciones"></telerik:GridBoundColumn>
                                    </Columns>
                                    <CommandItemTemplate>
                                    </CommandItemTemplate>
                                </MasterTableView>
                                <ClientSettings AllowExpandCollapse="True">
                                    <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                                </ClientSettings>
                            </telerik:RadGrid>
                        </div>
                        <div class="tab-pane fade" id="04">Disparo P.Dominante
                            <telerik:RadGrid ID="RadGrid4" runat="server" RenderMode="Auto" AllowPaging="False" AutoGenerateColumns="False" ShowHeadersWhenNoRecords="true">  
                                <MasterTableView DataKeyNames="ID">
                                    <HeaderStyle Width="200px" />
                                    <Columns>
                                        <telerik:GridBoundColumn HeaderText="Nº" DataField="ID"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn HeaderText="Nombre" Datafield="Nombre"></telerik:GridBoundColumn>
                                        <telerik:GridTemplateColumn HeaderText="1" HeaderStyle-Width="5%" UniqueName="1"> 
                                            <ItemTemplate>
                                            <telerik:RadButton ID="btnToggle1" runat="server" ToggleType="CheckBox" ButtonType="LinkButton">
                                                <togglestates>
                                                <telerik:RadButtonToggleState Text="A" PrimaryIconCssClass="rbToggleCheckboxChecked" />
                                                <telerik:RadButtonToggleState Text="A" PrimaryIconCssClass="rbToggleCheckbox" />
                                            </togglestates>
                                            </telerik:RadButton>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn> 
                                          <telerik:GridTemplateColumn HeaderText="2" HeaderStyle-Width="5%" UniqueName="2"> 
                                         <ItemTemplate>
                                       
                                            <telerik:RadButton ID="RadButton1" runat="server" ToggleType="CheckBox" ButtonType="LinkButton">
                                            <ToggleStates>
                                                <telerik:RadButtonToggleState Text="B" PrimaryIconCssClass="rbToggleCheckboxChecked" />
                                                <telerik:RadButtonToggleState Text="B" PrimaryIconCssClass="rbToggleCheckbox" />
                                            </ToggleStates>
                                        </telerik:RadButton>
                                         </ItemTemplate>
                                        </telerik:GridTemplateColumn> 
                                             <telerik:GridTemplateColumn HeaderText="3" HeaderStyle-Width="5%" UniqueName="3"> 
                                              <ItemTemplate>
                                                                           
                                            <telerik:RadButton ID="RadButton2" runat="server" ToggleType="CheckBox" ButtonType="LinkButton">
                                            <ToggleStates>
                                                <telerik:RadButtonToggleState Text="C" PrimaryIconCssClass="rbToggleCheckboxChecked" />
                                                <telerik:RadButtonToggleState Text="C" PrimaryIconCssClass="rbToggleCheckbox" />
                                            </ToggleStates>
                                        </telerik:RadButton>
                                                                                    </ItemTemplate>
                                        </telerik:GridTemplateColumn> 
                                               <telerik:GridTemplateColumn HeaderText="4" HeaderStyle-Width="5%" UniqueName="4"> 
                                                   <ItemTemplate>
                                                                         
                                            <telerik:RadButton ID="RadButton3" runat="server" ToggleType="CheckBox" ButtonType="LinkButton">
                                            <ToggleStates>
                                                <telerik:RadButtonToggleState Text="D" PrimaryIconCssClass="rbToggleCheckboxChecked" />
                                                <telerik:RadButtonToggleState Text="D" PrimaryIconCssClass="rbToggleCheckbox" />
                                            </ToggleStates>
                                        </telerik:RadButton>
                                                                                    </ItemTemplate>
                                        </telerik:GridTemplateColumn> 
                                         <telerik:GridBoundColumn HeaderText="Observaciones" UniqueName="Observaciones"></telerik:GridBoundColumn>
                                    </Columns>
                                    <CommandItemTemplate>
                                    </CommandItemTemplate>
                                </MasterTableView>
                                <ClientSettings AllowExpandCollapse="True">
                                    <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                                </ClientSettings>
                            </telerik:RadGrid>
                        </div>
                        <div class="tab-pane fade" id="05">Disparo P. NO Dominante
                            <telerik:RadGrid ID="RadGrid5" runat="server" RenderMode="Auto" AllowPaging="False" AutoGenerateColumns="False" ShowHeadersWhenNoRecords="true">  
                                <MasterTableView DataKeyNames="ID">
                                    <HeaderStyle Width="200px" />
                                   <Columns>
                                        <telerik:GridBoundColumn HeaderText="Nº" DataField="ID"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn HeaderText="Nombre" Datafield="Nombre"></telerik:GridBoundColumn>
                                        <telerik:GridTemplateColumn HeaderText="1" HeaderStyle-Width="5%" UniqueName="1"> 
                                            <ItemTemplate>
                                            <telerik:RadButton ID="btnToggle2" runat="server" ToggleType="CheckBox" ButtonType="LinkButton">
                                                <togglestates>
                                                <telerik:RadButtonToggleState Text="A" PrimaryIconCssClass="rbToggleCheckboxChecked" />
                                                <telerik:RadButtonToggleState Text="A" PrimaryIconCssClass="rbToggleCheckbox" />
                                            </togglestates>
                                            </telerik:RadButton>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn> 
                                          <telerik:GridTemplateColumn HeaderText="2" HeaderStyle-Width="5%" UniqueName="2"> 
                                         <ItemTemplate>
                                       
                                            <telerik:RadButton ID="RadButton12" runat="server" ToggleType="CheckBox" ButtonType="LinkButton">
                                            <ToggleStates>
                                                <telerik:RadButtonToggleState Text="B" PrimaryIconCssClass="rbToggleCheckboxChecked" />
                                                <telerik:RadButtonToggleState Text="B" PrimaryIconCssClass="rbToggleCheckbox" />
                                            </ToggleStates>
                                        </telerik:RadButton>
                                         </ItemTemplate>
                                        </telerik:GridTemplateColumn> 
                                             <telerik:GridTemplateColumn HeaderText="3" HeaderStyle-Width="5%" UniqueName="3"> 
                                              <ItemTemplate>
                                                                           
                                            <telerik:RadButton ID="RadButton22" runat="server" ToggleType="CheckBox" ButtonType="LinkButton">
                                            <ToggleStates>
                                                <telerik:RadButtonToggleState Text="C" PrimaryIconCssClass="rbToggleCheckboxChecked" />
                                                <telerik:RadButtonToggleState Text="C" PrimaryIconCssClass="rbToggleCheckbox" />
                                            </ToggleStates>
                                        </telerik:RadButton>
                                                                                    </ItemTemplate>
                                        </telerik:GridTemplateColumn> 
                                               <telerik:GridTemplateColumn HeaderText="4" HeaderStyle-Width="5%" UniqueName="4"> 
                                                   <ItemTemplate>
                                                                         
                                            <telerik:RadButton ID="RadButton32" runat="server" ToggleType="CheckBox" ButtonType="LinkButton">
                                            <ToggleStates>
                                                <telerik:RadButtonToggleState Text="D" PrimaryIconCssClass="rbToggleCheckboxChecked" />
                                                <telerik:RadButtonToggleState Text="D" PrimaryIconCssClass="rbToggleCheckbox" />
                                            </ToggleStates>
                                        </telerik:RadButton>
                                                                                    </ItemTemplate>
                                        </telerik:GridTemplateColumn> 
                                        <telerik:GridBoundColumn HeaderText="Observaciones" UniqueName="Observaciones"></telerik:GridBoundColumn>
                                    </Columns>
                                    <CommandItemTemplate>
                                    </CommandItemTemplate>
                                </MasterTableView>
                                <ClientSettings AllowExpandCollapse="True">
                                    <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                                </ClientSettings>
                            </telerik:RadGrid>
                        </div>
                        <div class="tab-pane fade" id="06">Pase en carrera

                            <telerik:RadGrid ID="RadGrid6" runat="server" RenderMode="Auto" AllowPaging="False" AutoGenerateColumns="False" ShowHeadersWhenNoRecords="true">  
                                <MasterTableView DataKeyNames="ID">
                                    <HeaderStyle Width="200px" />
                                        <Columns>
                                        <telerik:GridBoundColumn HeaderText="Nº" DataField="ID"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn HeaderText="Nombre" Datafield="Nombre"></telerik:GridBoundColumn>
                                           <%--  <telerik:GridBoundColumn HeaderText="Secs" Datafield="Secs"></telerik:GridBoundColumn>
                                             <telerik:GridBoundColumn HeaderText="Decs" Datafield="Decs"></telerik:GridBoundColumn>--%>
                                        <telerik:GridTemplateColumn HeaderText="1" HeaderStyle-Width="5%" UniqueName="1"> 
                                            <ItemTemplate>
                                            <telerik:RadButton ID="btnToggle3" runat="server" ToggleType="CheckBox" ButtonType="LinkButton">
                                                <togglestates>
                                                <telerik:RadButtonToggleState Text="A" PrimaryIconCssClass="rbToggleCheckboxChecked" />
                                                <telerik:RadButtonToggleState Text="A" PrimaryIconCssClass="rbToggleCheckbox" />
                                            </togglestates>
                                            </telerik:RadButton>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn> 
                                          <telerik:GridTemplateColumn HeaderText="2" HeaderStyle-Width="5%" UniqueName="2"> 
                                         <ItemTemplate>
                                       
                                            <telerik:RadButton ID="RadButton13" runat="server" ToggleType="CheckBox" ButtonType="LinkButton">
                                            <ToggleStates>
                                                <telerik:RadButtonToggleState Text="B" PrimaryIconCssClass="rbToggleCheckboxChecked" />
                                                <telerik:RadButtonToggleState Text="B" PrimaryIconCssClass="rbToggleCheckbox" />
                                            </ToggleStates>
                                        </telerik:RadButton>
                                         </ItemTemplate>
                                        </telerik:GridTemplateColumn> 
                                             <telerik:GridTemplateColumn HeaderText="3" HeaderStyle-Width="5%" UniqueName="3"> 
                                              <ItemTemplate>
                                                                           
                                            <telerik:RadButton ID="RadButton23" runat="server" ToggleType="CheckBox" ButtonType="LinkButton">
                                            <ToggleStates>
                                                <telerik:RadButtonToggleState Text="C" PrimaryIconCssClass="rbToggleCheckboxChecked" />
                                                <telerik:RadButtonToggleState Text="C" PrimaryIconCssClass="rbToggleCheckbox" />
                                            </ToggleStates>
                                        </telerik:RadButton>
                                        </ItemTemplate>
                                        </telerik:GridTemplateColumn> 
                                             <telerik:GridBoundColumn HeaderText="Observaciones" UniqueName="Observaciones"></telerik:GridBoundColumn>
                                    </Columns>
                           
                                    <CommandItemTemplate>
                                    </CommandItemTemplate>
                                </MasterTableView>
                                <ClientSettings AllowExpandCollapse="True">
                                    <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                                </ClientSettings>
                            </telerik:RadGrid>
                        </div>
                        <div class="tab-pane fade" id="07">Toques balón
                            <telerik:RadGrid ID="RadGrid7" runat="server" RenderMode="Auto" AllowPaging="False" AutoGenerateColumns="False" ShowHeadersWhenNoRecords="true">  
                                <MasterTableView DataKeyNames="ID">
                                    <HeaderStyle Width="200px" />
                                    <Columns>
                                        <telerik:GridBoundColumn HeaderText="Nº" DataField="ID"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn HeaderText="Nombre" Datafield="Nombre"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn HeaderText="Total (Nº)" DataField="Total"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn HeaderText="Partes (Nº)" DataField="Partes"></telerik:GridBoundColumn>
                                         <telerik:GridBoundColumn HeaderText="Observaciones" UniqueName="Observaciones"></telerik:GridBoundColumn>
                                    </Columns>
                                    <CommandItemTemplate>
                                    </CommandItemTemplate>
                                </MasterTableView>
                                <ClientSettings AllowExpandCollapse="True">
                                    <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                                </ClientSettings>
                            </telerik:RadGrid>
                        </div>
                        <div class="tab-pane fade" id="08">Circuito
                            <telerik:RadGrid ID="RadGrid8" runat="server" RenderMode="Auto" AllowPaging="False" AutoGenerateColumns="False" ShowHeadersWhenNoRecords="true">  
                                <MasterTableView DataKeyNames="ID">
                                    <HeaderStyle Width="200px" />
                                    <Columns>
                                        <telerik:GridBoundColumn HeaderText="Nº" DataField="ID"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn HeaderText="Nombre" Datafield="Nombre"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn HeaderText="Segs" DataField="Segs"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn HeaderText="Decs" DataField="Decs"></telerik:GridBoundColumn>
                                         <telerik:GridBoundColumn HeaderText="PICA" DataField="PICA"></telerik:GridBoundColumn>
                                         <telerik:GridBoundColumn HeaderText="CONO" DataField="CONO"></telerik:GridBoundColumn>
                                         <telerik:GridBoundColumn HeaderText="VALLA" DataField="VALLA"></telerik:GridBoundColumn>
                                         <telerik:GridBoundColumn HeaderText="CHINO" DataField="CHINO"></telerik:GridBoundColumn>
                                         <telerik:GridBoundColumn HeaderText="Observaciones" UniqueName="Observaciones"></telerik:GridBoundColumn>
                                    </Columns>
                                    <CommandItemTemplate>
                                    </CommandItemTemplate>
                                </MasterTableView>
                                <ClientSettings AllowExpandCollapse="True">
                                    <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                                </ClientSettings>
                            </telerik:RadGrid>
                        </div>
                        <div class="tab-pane fade" id="09">Remate de cabeza

                            <telerik:RadGrid ID="RadGrid9" runat="server" RenderMode="Auto" AllowPaging="False" AutoGenerateColumns="False" ShowHeadersWhenNoRecords="true">  
                                <MasterTableView DataKeyNames="ID">
                                    <HeaderStyle Width="200px" />
                                    <Columns>
                                        <telerik:GridBoundColumn HeaderText="Nº" DataField="ID"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn HeaderText="Nombre" Datafield="Nombre"></telerik:GridBoundColumn>
                                        <telerik:GridTemplateColumn HeaderText="1" HeaderStyle-Width="5%" UniqueName="1"> 
                                            <ItemTemplate>
                                                 <telerik:RadComboBox  id="RadComboBox24" 
                                                                        runat="server"> 
                                                    <Items>   
                                                        <telerik:RadComboBoxItem runat="server" Text="No remata" />   
                                                        <telerik:RadComboBoxItem runat="server" Text="A" />   
                                                        <telerik:RadComboBoxItem runat="server" Text="B" />   
                                                        <telerik:RadComboBoxItem runat="server" Text="C" /> 
                                                        <telerik:RadComboBoxItem runat="server" Text="D" /> 
                                                        <telerik:RadComboBoxItem runat="server" Text="F" /> 
                                                    </Items>
                                                </telerik:RadComboBox>
                                                </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                         <telerik:GridTemplateColumn HeaderText="2 remate" HeaderStyle-Width="5%" UniqueName="1"> 
                                            <ItemTemplate>
                                                 <telerik:RadComboBox  id="RadComboBoxw24" 
                                                                        runat="server"> 
                                                    <Items>   
                                                        <telerik:RadComboBoxItem runat="server" Text="No remata" />   
                                                        <telerik:RadComboBoxItem runat="server" Text="A" />   
                                                        <telerik:RadComboBoxItem runat="server" Text="B" />   
                                                        <telerik:RadComboBoxItem runat="server" Text="C" /> 
                                                        <telerik:RadComboBoxItem runat="server" Text="D" /> 
                                                        <telerik:RadComboBoxItem runat="server" Text="F" /> 
                                                    </Items>
                                                </telerik:RadComboBox>
                                                </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                         <telerik:GridTemplateColumn HeaderText="3" HeaderStyle-Width="5%" UniqueName="1"> 
                                            <ItemTemplate>
                                                 <telerik:RadComboBox  id="RadComboBoxq24" 
                                                                        runat="server"> 
                                                    <Items>   
                                                        <telerik:RadComboBoxItem runat="server" Text="No remata" />   
                                                        <telerik:RadComboBoxItem runat="server" Text="A" />   
                                                        <telerik:RadComboBoxItem runat="server" Text="B" />   
                                                        <telerik:RadComboBoxItem runat="server" Text="C" /> 
                                                        <telerik:RadComboBoxItem runat="server" Text="D" /> 
                                                        <telerik:RadComboBoxItem runat="server" Text="F" /> 
                                                    </Items>
                                                </telerik:RadComboBox>
                                                </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                         <telerik:GridBoundColumn HeaderText="Observaciones" UniqueName="Observaciones"></telerik:GridBoundColumn>
                                    </Columns>
                                    <CommandItemTemplate>
                                    </CommandItemTemplate>
                                </MasterTableView>
                                <ClientSettings AllowExpandCollapse="True">
                                    <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                                </ClientSettings>
                            </telerik:RadGrid>
                        </div>
                        <div class="tab-pane fade" id="10">Remate con el pie derecho
                            <telerik:RadGrid ID="RadGrid10" runat="server" RenderMode="Auto" AllowPaging="False" AutoGenerateColumns="False" ShowHeadersWhenNoRecords="true">  
                                <MasterTableView DataKeyNames="ID">
                                    <HeaderStyle Width="200px" />
                                    <Columns>
                                        <telerik:GridBoundColumn HeaderText="Nº" DataField="ID"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn HeaderText="Nombre" Datafield="Nombre"></telerik:GridBoundColumn>
                                         <telerik:GridTemplateColumn HeaderText="DER" HeaderStyle-Width="5%" UniqueName="1"> 
                                            <ItemTemplate>
                                                   <telerik:RadComboBox  id="RadComboBox35" 
                                                                        runat="server"> 
                                                    <Items>   
                                                   <telerik:RadComboBoxItem runat="server" Text="No remata" /> 
                                                        <telerik:RadComboBoxItem runat="server" Text="A" />   
                                                        <telerik:RadComboBoxItem runat="server" Text="B" />   
                                                        <telerik:RadComboBoxItem runat="server" Text="C" /> 
                                                         <telerik:RadComboBoxItem runat="server" Text="D" />
                                                          <telerik:RadComboBoxItem runat="server" Text="F" /> 
                                                    </Items>
                                                </telerik:RadComboBox>
                                                </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                          <telerik:GridTemplateColumn HeaderText="DER2" HeaderStyle-Width="5%" UniqueName="1"> 
                                            <ItemTemplate>
                                                   <telerik:RadComboBox  id="RadComboBox315" 
                                                                        runat="server"> 
                                                    <Items>   
                                                        <telerik:RadComboBoxItem runat="server" Text="No remata" /> 
                                                        <telerik:RadComboBoxItem runat="server" Text="A" />   
                                                        <telerik:RadComboBoxItem runat="server" Text="B" />   
                                                        <telerik:RadComboBoxItem runat="server" Text="C" /> 
                                                         <telerik:RadComboBoxItem runat="server" Text="D" />
                                                          <telerik:RadComboBoxItem runat="server" Text="F" /> 
                                                    </Items>
                                                </telerik:RadComboBox>
                                                </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                           <telerik:GridTemplateColumn HeaderText="DER3" HeaderStyle-Width="5%" UniqueName="1"> 
                                            <ItemTemplate>
                                                   <telerik:RadComboBox  id="RadComboBox315" 
                                                                        runat="server"> 
                                                    <Items>   
                                                        <telerik:RadComboBoxItem runat="server" Text="No remata" /> 
                                                        <telerik:RadComboBoxItem runat="server" Text="A" />   
                                                        <telerik:RadComboBoxItem runat="server" Text="B" />   
                                                        <telerik:RadComboBoxItem runat="server" Text="C" /> 
                                                         <telerik:RadComboBoxItem runat="server" Text="D" />
                                                          <telerik:RadComboBoxItem runat="server" Text="F" /> 
                                                    </Items>
                                                </telerik:RadComboBox>
                                                </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                      
                                    </Columns>
                                    <CommandItemTemplate>
                                    </CommandItemTemplate>
                                </MasterTableView>
                                <ClientSettings AllowExpandCollapse="True">
                                    <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                                </ClientSettings>
                            </telerik:RadGrid>
                        </div>
            <%--            REMATE CON EL PIE IZQUIERDO--%>
                        <div class="tab-pane fade" id="11">Potencia de disparo
                           <telerik:RadGrid ID="RadGrid11" runat="server" RenderMode="Auto" AllowPaging="False" AutoGenerateColumns="False" ShowHeadersWhenNoRecords="true">  
                                <MasterTableView DataKeyNames="ID">
                                    <HeaderStyle Width="200px" />
                                    <Columns>
                                        <telerik:GridBoundColumn HeaderText="Nº" DataField="ID"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn HeaderText="Nombre" Datafield="Nombre"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn HeaderText="Segs" DataField="Segs"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn HeaderText="Decs" DataField="Decs"></telerik:GridBoundColumn>
                                    </Columns>
                                    <CommandItemTemplate>
                                    </CommandItemTemplate>
                                </MasterTableView>
                                <ClientSettings AllowExpandCollapse="True">
                                    <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                                </ClientSettings>
                            </telerik:RadGrid>
                        </div>
                        <div class="tab-pane fade" id="12">Resumen
                           <telerik:RadGrid ID="RadGrid12" runat="server" RenderMode="Auto" AllowPaging="False" AutoGenerateColumns="False" ShowHeadersWhenNoRecords="true">  
                                <MasterTableView DataKeyNames="ID">
                                    <HeaderStyle Width="200px" />
                                    <Columns>
                                        <telerik:GridBoundColumn HeaderText="Nº" DataField="ID"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn HeaderText="Nombre" Datafield="Nombre"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn HeaderText="De Pie (cm)" DataField="Date"></telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn HeaderText="Salto (cm)" DataField="Date"></telerik:GridBoundColumn>
                                    </Columns>
                                    <CommandItemTemplate>
                                    </CommandItemTemplate>
                                </MasterTableView>
                                <ClientSettings AllowExpandCollapse="True">
                                    <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                                </ClientSettings>
                            </telerik:RadGrid>
                        </div>
                    </div>
             </div>

        </div>
    </nav>
</div>
   
    </form>
</body>
</html>                                		