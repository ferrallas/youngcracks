﻿<%@ Application Codebehind="Global.asax.cs" Inherits="YC_Collect_Data.Global" Language="C#" %>

<script runat="server" language="c#">
protected void Application_Error(Object sender, EventArgs e){
Exception ex = Server.GetLastError();
Server.ClearError();

Response.Write("<pre>");
Response.Write(Environment.NewLine);
Response.Write("Caught Exception: " + ex.ToString() + Environment.NewLine);

if (ex.InnerException != null){
Response.Write(Environment.NewLine);
Response.Write("Inner Exception: " + ex.InnerException.ToString() + Environment.NewLine);
Response.Write(Environment.NewLine);
}

System.Security.SecurityException ex_security = ex as System.Security.SecurityException;

if (ex_security != null){
Response.Write(Environment.NewLine);
Response.Write("Security Exception Details:");
Response.Write(Environment.NewLine);
Response.Write("===========================");
Response.Write(Environment.NewLine);
Response.Write("PermissionState: " + ex_security.PermissionState + Environment.NewLine);
Response.Write("PermissionType: " + ex_security.PermissionType + Environment.NewLine);
Response.Write("RefusedSet: " + ex_security.RefusedSet + Environment.NewLine);
}

Response.Write("</pre>");
}
</script>