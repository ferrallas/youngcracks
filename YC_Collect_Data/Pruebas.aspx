﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Pruebas.aspx.cs" Inherits="YC_Collect_Data.Pruebas" %>

<!DOCTYPE html>

<html lang="en">
<head runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title><%: Page.Title %> -</title>

    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/bundles/modernizr") %>
    </asp:PlaceHolder>
    <webopt:bundlereference runat="server" path="~/Content/css" />
    <link href="~/favicon.ico" rel="shortcut icon" type="image/x-icon" />
    <meta name="description" content="The description of my page" />
</head>
<body>
    <form runat="server">
        <asp:ScriptManager runat="server">
            <Scripts>
                <%--To learn more about bundling scripts in ScriptManager see http://go.microsoft.com/fwlink/?LinkID=301884 --%>
                <%--Framework Scripts--%>
                <asp:ScriptReference Name="MsAjaxBundle" />
                <asp:ScriptReference Name="jquery" />
                <asp:ScriptReference Name="bootstrap" />
                <asp:ScriptReference Name="respond" />
                <asp:ScriptReference Name="WebForms.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebForms.js" />
                <asp:ScriptReference Name="WebUIValidation.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebUIValidation.js" />
                <asp:ScriptReference Name="MenuStandards.js" Assembly="System.Web" Path="~/Scripts/WebForms/MenuStandards.js" />
                <asp:ScriptReference Name="GridView.js" Assembly="System.Web" Path="~/Scripts/WebForms/GridView.js" />
                <asp:ScriptReference Name="DetailsView.js" Assembly="System.Web" Path="~/Scripts/WebForms/DetailsView.js" />
                <asp:ScriptReference Name="TreeView.js" Assembly="System.Web" Path="~/Scripts/WebForms/TreeView.js" />
                <asp:ScriptReference Name="WebParts.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebParts.js" />
                <asp:ScriptReference Name="Focus.js" Assembly="System.Web" Path="~/Scripts/WebForms/Focus.js" />
                <asp:ScriptReference Name="WebFormsBundle" />
                <%--Site Scripts--%>
            </Scripts>
        </asp:ScriptManager>

        <div class="navbar navbar-inverse navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" runat="server" href="~/">Hora: <%: DateTime.Now %> </a>
                </div>
                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li><a runat="server" href="~/">Inicio</a></li>
                        <li><a runat="server" href="~/About">Instrucciones</a></li>
                        <li><a runat="server" href="~/Contact">Contacto</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="container body-content">
        <h1>Test Youngcracks.com</h1>
        <p class="lead"></p>
    </div>

    <div class="row">
         <div class="container">
           <div id="countryList" class='highlight'></div>
             <script type="text/javascript">

                 var countryData = {
                        countryList:
                            [{country: 'Grupo 1', url: '#' },
                             {country: 'Grupo 2', url: '#' },
                             {country: 'Grupo 3', url: '#' },
                             {country: 'Grupo 4', url: '#' }]
                 }

                 function getParameterByName(name) {
                     name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
                     var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                         results = regex.exec(location.search);
                     return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
                 }

                 var total = getParameterByName('Grupos');

                function buildList(countryData) {
                    var Name = null;

                    var ListContainer = document.getElementById('countryList');


                    countryLoaded = true;
                    var countryList = document.createElement("ul");
                    countryList.className = 'nav nav-pills'


                    for (var i = 1; i <= total; i++)
                    {
                        var countryListItem = document.createElement("li");
                        var countryLink = document.createElement("a");

                        Name = "Grupo" + i;

                        countryLink.href = "javascript:link(" + i + ")";
                        countryLink.innerText = Name;

                        countryListItem.id = "CL" + (i+1);
                        countryListItem.appendChild(countryLink);
                        countryList.appendChild(countryListItem);
                    }

                    ListContainer.appendChild(countryList);
                }

                    
                buildList(countryData);


                function link(i) 
                {
                    var RS = getParameterByName('RS');
                    if (RS == 1) { $('#frame').attr('src', 'Grupop.aspx?&GRN=' + i); }
                    else { $('#frame').attr('src', 'Grupoj.aspx?&GRN=' + i); }
                }

             </script>

             <div id="mydiv"><iframe id="frame"  frameborder="0" width="100%" height="900">
             </iframe></div>
        </div>
        
        </div>
            <hr />
            <footer>
                <p>&copy; <%: DateTime.Now.Year %> - Youngcracks.com</p>
            </footer>
    </form>
</body>
</html>

